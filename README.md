# Traefik - Kustomize + Kubernetes skeleton

This project is a another all-in-one ingress controller for Kubernetes. The goal is to limit the project-side configuration to its simplest expression: only the domain name for a service. This project is based on the use of Traefik and Letsencrypt. The configuration is done via a .env file, which is used for secrets generation via "kustomize" command. You can use it on a development workstation via minikube as on a production cluster.

## Prerequisites

- Docker >= 18
- Kubectl >= 1.9
- a Kubernetes cluster (GCE / AWS / minikube / etc...) 

## Special prerequisites for Minikube users on Ubuntu / Debian

On a local minikube, you can face a deny from letsencrypt servers, because you request too many times, if you reboot several times. This is due to the storage provisioner that store by default nside /tmp/hostpath-provisioner, and that path is flushed every time at startup. In order to bypass this behavior, you can use this little systemd service:

```bash
sudo mkdir -p /var/lib/kubernetes
cat <<EOF | sudo tee /etc/systemd/system/kube-link-persistent-dir.service
[Unit]
Description=Make a symbolic link before Kubernetes startup
Before=kubelet.service
[Service]
Type=oneshot
ExecStart=/bin/sh -c 'mkdir -p /tmp/hostpath-provisioner && mount -o bind /var/lib/kubernetes /tmp/hostpath-provisioner'
TimeoutSec=0
RemainAfterExit=yes
SysVStartPriority=10
[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
sudo systemctl enable kube-link-persistent-dir.service
sudo systemctl start kube-link-persistent-dir.service
# restart kubelet or your whole computer
sudo systemctl restart kubelet.service
```


## Setup

```bash
git clone git@gitlab.com:d01t/kubernetes/traefik.git
cd traefik
cp .env.dist .env
```

Edit .env file and fill with your email.
If your cluster is not accessible by Let's Encrypt validation servers, you have to use DNS validation by
modifying DNS_CHALLENGE=true , selecting a DNS provider and fill in correct credentials.
See https://docs.traefik.io/v2.0/https/acme/#providers for your DNS configuration credentials.

```
kubectl create namespace traefik
kubectl apply -n traefik -k .
# 2 times as templates define new kinds
kubectl apply -n traefik -k .
```

If you use minikube, you can port forward directly to the ingress controller service with following command:

```bash
sudo kubectl --namespace=kube-system port-forward --address 0.0.0.0 service/traefik-ingress-service 80:80 443:443
```

And if you want to start this command every time your computer boots, on Debian / Ubuntu, you can use systemd:

```bash
cat <<EOF | sudo tee /usr/local/bin/kube-port-forward.sh 
#!/bin/bash
KUBEARGS=(
    "--kubeconfig=\$(ls /home/*/.kube/config | sed -n '1p')"
    "--context=minikube"
    "--namespace=traefik"
)
while ! kubectl \${KUBEARGS[@]} get services traefik
do
    sleep 1
done
sleep 1
while [[ \$(kubectl \${KUBEARGS[@]} get pods -l name=traefik -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != 'True' ]]
do
    sleep 1
done
sleep 1
while ! kubectl \${KUBEARGS[@]} port-forward --address 0.0.0.0 service/traefik 80:80 443:443
do
    sleep 1
done
EOF
sudo chmod +x /usr/local/bin/kube-port-forward.sh
cat <<EOF | sudo tee /etc/systemd/system/kubectl-port-forward.service 
[Unit]
Description=Kubernetes port forwarding to traefik ingress controller
After=network.target kubelet.service
ConditionPathExists=/usr/bin/kubectl
[Service]
Type=simple
ExecStart=/usr/local/bin/kube-port-forward.sh
TimeoutSec=0
RemainAfterExit=yes
SysVStartPriority=99
[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
sudo systemctl enable kubectl-port-forward.service
sudo systemctl start kubectl-port-forward.service
```
